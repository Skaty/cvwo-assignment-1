# CVWO Assignment 1 #
The first assignment for Computing for Voluntary Welfare Organisations (CVWO).

## Author ##

I am Syed Abdullah, matriculation number: A0141052Y.

## Getting Started ##

This web application is built using Laravel, so some understanding of Laravel and associated tools such as `composer` and `artisan` is required.

Do note that you would need to configure the `.env` file (using `.env.example`) as an example.

## TODO ##

Continuous Integration using Semaphore.