<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // parent category, unable to name this more intinutively, due to
            // Laravel's insistence on [table_name]_id format for One-to-One
            // to work
            $table->integer('parent_category_id')->unsigned()->nullable();
            $table->foreign('parent_category_id')->references('id')
                  ->on('categories')->onDelete('set null');
            $table->timestamps();
        });

        Schema::table('posts', function (Blueprint $table) {
          $table->integer('category_id')->unsigned();
          $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
