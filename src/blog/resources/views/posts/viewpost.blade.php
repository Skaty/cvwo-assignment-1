@extends('layouts.app')

@section('title', $post->title)

@section('header')
<div class="post-heading">
    <h1>{{ $post->title }}</h1>
    <span class="subheading">there is currently no subtitle support</span>
    <span class="meta">Posted by <a href="#">{{$post->author->name}}</a> on {{ date("M d, Y",$post->created_at->getTimestamp()) }}</span>
</div>
@endsection

@section('content')
{{ $post->content }}

<p class="post-meta"><b>Category:</b> {{ $post->category->name }}</p>
<hr>
<h2>Comments</h2>
@forelse($post->comments->all() as $comment)
  <hr/>
  <p>From <b>{{ $comment->name }}</b> on {{ date("M d, Y",$post->created_at->getTimestamp()) }}</p>
  <p>{{ $comment->content }}</h4>
@empty
  <p>There are currently no comments here! Why not be the first?</p>
@endforelse
<hr/>
<h3>Post comment</h3>
<form method="POST" action="{{ route('frontend::postComment', ['id' => $post->id ]) }}">
  {!! csrf_field() !!}
  <div class="form-group">
    <input type="text" class="form-control" id="titleField" placeholder="Your Name" name="name" value="">
  </div>
  <div class="form-group">
    <textarea name="content" placeholder="Your Comment..." class="form-control" id="bodyField" rows="5"></textarea>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
@endsection
