@extends('layouts.app')

@section('title', 'Main')

@section('header')
<div class="site-heading">
    <h1>CVWO Blog</h1>
    <hr class="small">
    <span class="subheading">An excellent CVWO assignment</span>
</div>
@endsection

@section('content')
@forelse ($posts as $post)
    <div class="post-preview">
      <a href="{{ route('frontend::getPost', ['id' => $post->id ]) }}">
        <h2 class="post-title">
          {{$post->title}}
        </h2>
        <h3 class="post-subtitle">
          there is currently no subtitle support
        </h3>
      </a>
      <p class="post-meta">Posted in <a href="#">{{ $post->category->name }}</a> by <a href="#">{{ $post->author->name }}</a> on {{ date("M d, Y",$post->created_at->getTimestamp()) }}</p>
    </div>
    <hr>
@empty
    <h2>The Blog currently has no posts!</h2>
@endforelse



<!-- Pager -->
<ul class="pager">
  @if ($posts->currentPage() > 1)
  <li class="previous">
    <a href="#">Newer Posts</a>
  </li>
  @endif
  @if ($posts->hasMorePages())
    <li class="next">
      <a href="{{$posts->nextPageUrl()}}">Older Posts &rarr;</a>
    </li>
  @endif
</ul>
@endsection
