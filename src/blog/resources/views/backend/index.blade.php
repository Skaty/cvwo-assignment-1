@extends('layouts.backend')

@section('css')
  <link href="//cdn.datatables.net/responsive/2.0.0/css/responsive.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
          <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Blog Posts
                        <div class="pull-right">
                                <a href="{{ route('backend::getPost')}}" class="btn btn-xs  btn-default"><i class="fa fa-pencil"></i> New Post</a>
                            </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="postTable">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Posted On</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @forelse($posts as $post)
                                    <tr>
                                        <td>{{ $post->title }}</td>
                                        <td>{{ date("D, d M Y H:i",$post->created_at->getTimestamp()) }}</td>
                                        <td>
                                          <div class="btn-group" role="group">
                                            <a class="btn btn-sm btn-primary" href="{{ route('backend::retrievePost', ['id' => $post->id ])}}" role="button"><i class="fa fa-pencil"></i> Edit</a>
                                            <a class="btn btn-sm btn-danger" href="#" role="button"><i class="fa fa-trash"></i> Delete</a>
                                          </div>
                                        </td>
                                    </tr>
                                    @empty
                                      <h3>There are currently no blog posts! Why not create one?</h3>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Categories
                        <div class="pull-right">
                                <a href="{{ route('backend::getCategory')}}" class="btn btn-xs  btn-default"><i class="fa fa-pencil"></i> New Category</a>
                            </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="categoriesTable">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($categories as $category)
                                    <tr>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                          <div class="btn-group" role="group">
                                            <a class="btn btn-sm btn-primary" href="{{ route('backend::retrieveCategory', ['id' => $category->id ])}}" role="button"><i class="fa fa-pencil"></i> Edit</a>
                                            <a class="btn btn-sm btn-danger" href="#" role="button"><i class="fa fa-trash"></i> Delete</a>
                                          </div>
                                        </td>
                                    </tr>
                                    @empty
                                      <h3>There are currently no categories! Why not create one?</h3>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->
          @endsection

@section('scripts')
  <script src="//cdn.datatables.net/responsive/2.0.0/js/dataTables.responsive.min.js"></script>

  <script>
  $(document).ready(function() {
      $('#postsTable').DataTable({
              responsive: true
      });

      $('#categoriesTable').DataTable({
              responsive: true
      });
  });
  </script>
@endsection
