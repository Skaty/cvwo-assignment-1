@extends('layouts.backend')

@section('css')
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        @if(isset($post)) Editing Post: {{ $post->id }} @else New Post @endif
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <form method="POST" action="@if(isset($post)) {!! route('backend::modifyPost', ["id" => $post->id]) !!} @else {!! route('backend::postPost') !!} @endif">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="titleField">Title</label>
            <input type="text" class="form-control" id="titleField" name="title" value="{{ $post->title or "" }}">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Category</label>
            <select class="form-control" name="category_id">
            @foreach($categories as $category)
              <option value="{{ $category->id }}" @if(isset($post) && ($post->category_id == $category->id)) selected @endif >{{ $category->name }}</option>
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <textarea name="content" class="form-control" id="bodyField" rows="5">{{ $post->content or "" }}</textarea>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('scripts')
@endsection
