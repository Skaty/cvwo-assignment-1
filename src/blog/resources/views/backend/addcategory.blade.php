@extends('layouts.backend')

@section('css')
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        @if(isset($category)) Editing Category: {{ $category->id }} @else New Category @endif
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <form method="POST" action="@if(isset($category)) {!! route('backend::modifyCategory', ["id" => $category->id]) !!} @else {!! route('backend::postCategory') !!} @endif">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="nameField">Category Name</label>
            <input type="text" class="form-control" id="nameField" name="name" value="{{ $category->name or "" }}">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('scripts')
@endsection
