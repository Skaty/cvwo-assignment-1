@extends('layouts.authlayout')

@section('title', 'Register')

@section('content')
<div class="login-panel panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Registration</h3>
  </div>
  <div class="panel-body">
    <form method="POST" action="{{route('register')}}" role="form">
      {{ csrf_field() }}
      <!-- fields as per Laravel (5.1) requirements for Auth views -->
      <fieldset>
        <div class="form-group">
          <input class="form-control" placeholder="Name" name="name" type="text" value="{{ old('name') }}" autofocus>
        </div>
        <div class="form-group">
          <input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
          <input class="form-control" placeholder="Password" name="password" type="password" value="">
        </div>
        <div class="form-group">
          <input class="form-control" placeholder="Confirm" name="password_confirmation" type="password" value="">
        </div>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Register">
      </fieldset>
    </form>
  </div>
</div>
@endsection
