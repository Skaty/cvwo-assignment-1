@extends('layouts.authlayout')

@section('title', 'Login')

@section('content')
<div class="login-panel panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Please Sign In</h3>
  </div>
  <div class="panel-body">
    <form  method="POST" action="{{route('login')}}" role="form">
      {{ csrf_field() }}
      <fieldset>
        <div class="form-group">
          <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
        </div>
        <div class="form-group">
          <input class="form-control" placeholder="Password" name="password" type="password" value="">
        </div>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
      </fieldset>
    </form>
  </div>
</div>
@endsection
