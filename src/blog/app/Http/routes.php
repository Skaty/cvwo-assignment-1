<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Routes for Authentication & Registration
Route::get('auth/login', ['as' => 'login',
                          'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', ['as' => 'logout',
                           'uses' => 'Auth\AuthController@getLogout']);
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'register',
                              'uses' => 'Auth\AuthController@postRegister']);

// Routes for blog
Route::group(['as' => 'frontend::'], function() {
  Route::post('/post/{id}/comment', ['as' => 'postComment', 'uses' => 'PostController@postComment']);
  Route::get('/post/{id}', ['as' => 'getPost', 'uses' => 'PostController@getPost']);
  Route::get('/', 'PostController@index');
});

// Routes for backend panel
Route::group(['prefix' => 'admin-panel','middleware' => 'auth', 'as' => 'backend::'], function () {
  Route::get('categories/new',  ['as' => 'getCategory', 'uses' => 'BackendController@getCategory']);
  Route::post('categories/new',  ['as' => 'postCategory', 'uses' => 'BackendController@postCategory']);
  Route::get('categories/{id}',  ['as' => 'retrieveCategory', 'uses' => 'BackendController@retrieveCategory']);
  Route::post('categories/{id}',  ['as' => 'modifyCategory', 'uses' => 'BackendController@updateCategory']);

  Route::get('posts/new', ['as' => 'getPost', 'uses' => 'BackendController@getPost']);
  Route::post('posts/new', ['as' => 'postPost', 'uses' => 'BackendController@postPost']);
  Route::get('posts/{id}',  ['as' => 'retrievePost', 'uses' => 'BackendController@retrievePost']);
  Route::post('posts/{id}',  ['as' => 'modifyPost', 'uses' => 'BackendController@updatePost']);

  Route::get('/', ['as' => 'index', 'uses' => 'BackendController@index']);
});
