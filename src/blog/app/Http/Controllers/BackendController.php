<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BackendController extends Controller
{
  /**
   * Constructor for BackednController
   *
   * @return void
  **/
  public function __construct()
  {
    //do nothing?
  }

  /**
   * Displays a list of all blog posts
   *
   * @param  Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $posts = Post::orderBy('created_at', 'desc')->get();
    $categories = Category::all();
    return view('backend.index', [
           'posts' => $posts,
           'categories' => $categories,
    ]);
  }

  /**
   * Shows the form for Add Post
   *
   * @param  Request $request
   * @return Response
   */
  public function getPost(Request $request)
  {
    $categories = Category::all();
    return view('backend.addpost', [
      'categories' => $categories,
    ]);
  }

  /**
   * Processes any POST request for 'Add Post'
   *
   * @param  Request $request
   * @return Response
   */
  public function postPost(Request $request)
  {
    $this->validate($request, [
        'title' => 'required|max:255',
        'content' => 'required',
        'category_id' => 'required|exists:categories,id',
    ]);

    $request->user()->posts()->create([
      'title' => $request->title,
      'content' => $request->content,
      'category_id' => $request->category_id,
    ]);

    return redirect()->route('backend::index');
  }

  /**
   * Shows form for Modify Post
   *
   * @param  Request $request, $id
   * @return Response
   */
  public function retrievePost(Request $request, $id)
  {
    $categories = Category::all();
    $post = Post::findOrFail($id);
    return view('backend.addpost', [
      'categories' => $categories,
      'post' => $post,
    ]);
  }

  /**
   * Processes any POST request for any post modification
   *
   * @param  Request $request, $id
   * @return Response
   */
  public function updatePost(Request $request, $id)
  {
    $this->validate($request, [
        'title' => 'required|max:255',
        'content' => 'required',
        'category_id' => 'required|exists:categories,id',
    ]);

    $post = Post::findOrFail($id);

    $post->update([
      'title' => $request->title,
      'content' => $request->content,
      'category_id' => $request->category_id,
    ]);

    return redirect()->route('backend::index');
  }

  /**
   * Shows the form for adding a new category
   *
   * @param  Request $request
   * @return Response
   */
  public function getCategory(Request $request)
  {
    $categories = Category::all();
    return view('backend.addcategory', [
      'categories' => $categories,
    ]);
  }

  /**
   * Processes any POST request for addition of a new category
   *
   * @param  Request $request
   * @return Response
   */
  public function postCategory(Request $request)
  {
    $this->validate($request, [
        'name' => 'required|unique:categories|max:255'
    ]);

    $newcat = new Category;

    $newcat->name = $request->name;

    $newcat->save();

    return redirect()->route('backend::index');
  }

  /**
   * Shows form when user modifies an existing category
   *
   * @param  Request $request, $id
   * @return Response
   */
  public function retrieveCategory(Request $request, $id)
  {
    $categories = Category::all();
    $category = Category::findOrFail($id);
    return view('backend.addcategory', [
      'categories' => $categories,
      'category' => $category,
    ]);
  }

  /**
   * Processes any POST request for any category modification
   *
   * @param  Request $request, $id
   * @return Response
   */
  public function updateCategory(Request $request, $id)
  {
    $this->validate($request, [
        'name' => 'required|unique:categories|max:255',
    ]);

    $category = Category::findOrFail($id);

    $category->update([
      'name' => $request->name,
    ]);

    return redirect()->route('backend::index');
  }
}
