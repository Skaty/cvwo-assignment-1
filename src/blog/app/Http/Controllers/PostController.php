<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Constructor for PostController
     *
     * @return void
    **/
    public function __construct()
    {
      //do nothing?
    }

    /**
     * Displays all blog posts, from most recent
     *
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
      $posts = Post::orderBy('created_at', 'desc')
                     ->paginate(5);
      return view('posts.index', [
             'posts' => $posts,
      ]);
    }

    /**
     * Displays a specific blog post
     *
     * @param  Request $request, $id
     * @return Response
     */
    public function getPost(Request $request, $id)
    {
      $post = Post::findOrFail($id);
      return view('posts.viewpost', [
             'post' => $post,
      ]);
    }

    /**
     * Handles a POST request for a new comment
     *
     * @param  Request $request, $id
     * @return Response
     */
     public function postComment(Request $request, $id)
     {
       $this->validate($request, [
           'name' => 'required|max:255',
           'content' => 'required',
       ]);
       $post = Post::findOrFail($id);
       $comment = Comment::create(['name' => $request->name,
                                   'content' => $request->content,
                                   'post_id' => $id,
                                  ]);
       return redirect()->route('frontend::getPost', ['id' => $id]);
     }
}
