<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['name', 'parent_category_id'];

  /**
   * Get posts in category
   */
   public function posts()
   {
     return $this->hasMany('App\Post');
   }

  /**
   * Get parent category
   */
   public function parent_category()
   {
     return $this->belongsTo('App\Category', 'parent_category_id');
   }

  /**
   * Get all subcategories of this category
   */
   public function subcategories()
   {
     return $this->hasMany('App\Category', 'parent_category_id');
   }
}
