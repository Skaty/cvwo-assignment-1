<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['title', 'content', 'category_id'];

  /**
   * Get category of post
   */
   public function category()
   {
     return $this->belongsTo('App\Category');
   }

  /**
   * Get author of post
   */
   public function author()
   {
     return $this->belongsTo('App\User', 'user_id');
   }

  /**
   * Get all comments associated with the post
   */
   public function comments()
   {
     return $this->hasMany('App\Comment');
   }
}
