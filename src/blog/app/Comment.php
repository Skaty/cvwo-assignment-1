<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['name','content', 'post_id'];

  /**
  * Gets the post that the comment is associated with
  */
  public function post()
  {
    return $this->belongsTo('App\Post');
  }

  /**
  * Gets the author of the comment
  */
  public function author()
  {
    return $this->belongsTo('App\User');
  }
}
